package juliantopo.com.motivapp.Model.DTO;

import org.bson.Document;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TaskDTO {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
    public static final String DELETE = "DELETE";
    public static final String ADD = "ADD_TASK_";

    // ---------------- Atributes ------------------------------------ //

    // User id
    private String userId;

    // Description
    private String description;

    // Type of task
    private String type;

    // date due
    private Date dateDue;

    // date created
    private Date dateCreated;

    // Task completed
    private Integer completed;

    // Firebase id user
    private String firebaseIdUser;

    // Object id (created by mongo)
    private String objectId;

    // Task priority
    private int priority;

    // Task hour duration
    private int hourDuration;

    // Task minute duration
    private int minuteDuration;

    // can be null, DELETE or ADD
    private String statusOnLocalStorage;

    // Builder for brand new motivation
    public TaskDTO(String pUserId, String pType, String pDescripcion, Integer pCompleted, String pFireBaseUser, Date pDate, int pPriority, int pHours, int pMinutes) {
        this.userId = pUserId;
        this.type = pType;
        this.dateDue = pDate;
        this.description = pDescripcion;
        this.completed = pCompleted;
        this.firebaseIdUser = pFireBaseUser;
        this.priority = pPriority;
        this.hourDuration = pHours;
        this.minuteDuration = pMinutes;
        this.dateCreated = new Date();
    }

    // Builder for an existing task in Mongo db
    public TaskDTO(Document taskDocument) {
        this.userId = taskDocument.getString("user_id");
        this.type = taskDocument.getString("type");
        this.dateDue = taskDocument.getDate("date_due");
        this.dateCreated = taskDocument.getDate("date_created");
        this.description = taskDocument.getString("description");
        this.completed = taskDocument.getInteger("completed");
        this.firebaseIdUser = taskDocument.getString("firebase_id_user");
        this.objectId = taskDocument.getObjectId("_id").toString();
//        this.minuteDuration = taskDocument.getInteger("minute_duration");
        //      this.hourDuration = taskDocument.getInteger("hour_duration");
        //    this.priority = taskDocument.getInteger("priority");
        this.statusOnLocalStorage = "NOTHING";
    }

    /**
     * Constructor for local storage
     */
    public TaskDTO(String line) {
        String[] attrs = line.split(";");
        this.type = attrs[0];
        this.description = attrs[2];
        this.completed = Integer.parseInt(attrs[4]);
        this.minuteDuration = Integer.parseInt(attrs[5]);
        this.hourDuration = Integer.parseInt(attrs[6]);
        this.priority = Integer.parseInt(attrs[7]);
        this.objectId = attrs[8];
        this.firebaseIdUser = attrs[9];
        this.userId = attrs[10];
        this.statusOnLocalStorage = attrs[11];
    }

    /**
     * Returns string to save in local storage file
     *
     * @return string of line to save
     */
    public String toStringLocalStorage() {
        return getType() + ";" + getDateDue()  + ";" + getDescription() + ";" + getDateCreated() + ";" + getCompleted() + ";" + getMinuteDuration() + ";" + getHourDuration() + ";" + getPriority()
                + ";" + getObjectId() + ";" + getFirebaseIdUser() + ";" + getUserId() + ";" + this.getStatusOnLocalStorage();
    }


    // -------------------------- Setters and getters ---------------------------------------//

    public String getUserId() {
        return userId;
    }

    public TaskDTO setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getType() {
        return type;
    }

    public TaskDTO setType(String type) {
        this.type = type;
        return this;
    }

    public Date getDateDue() {
        return dateDue;
    }

    public TaskDTO setDateDue(Date dateDue) {
        this.dateDue = dateDue;
        return this;
    }

    public Integer getCompleted() {
        return completed;
    }

    public TaskDTO setCompleted(Integer completed) {
        this.completed = completed;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public TaskDTO setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getFirebaseIdUser() {
        return firebaseIdUser;
    }

    public TaskDTO setFirebaseIdUser(String firebaseIdUser) {
        this.firebaseIdUser = firebaseIdUser;
        return this;
    }

    public String getObjectId() {
        return objectId;
    }

    public TaskDTO setObjectId(String objectId) {
        this.objectId = objectId;
        return this;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getHourDuration() {
        return hourDuration;
    }

    public void setHourDuration(int hourDuration) {
        this.hourDuration = hourDuration;
    }

    public int getMinuteDuration() {
        return minuteDuration;
    }

    public void setMinuteDuration(int minuteDuration) {
        this.minuteDuration = minuteDuration;
    }

    public String getStatusOnLocalStorage() {
        return statusOnLocalStorage;
    }

    public void setStatusOnLocalStorage(String statusOnLocalStorage) {
        this.statusOnLocalStorage = statusOnLocalStorage;
    }

    // ---------------------- util ------------------------------------//

    /**
     * Transforms the element to a mongo db document
     *
     * @return Document that represents the DTO
     */
    public Document getMongoDbObject() {
        Document doc = new Document();
        doc.append("user_id", getUserId());
        doc.append("date_due", getDateDue());
        doc.append("description", getDescription());
        doc.append("type", getType());
        doc.append("completed", getCompleted());
        doc.append("firebase_id_user", getFirebaseIdUser());
        doc.append("date_due", getDateDue());
        doc.append("date_created", getDateCreated());
        doc.append("priority", getPriority());
        doc.append("hour_duration", getHourDuration());
        doc.append("minute_duration", getMinuteDuration());
        return doc;
    }

    @Override
    public String toString() {
        return "TaskDTO{" +
                "dateFormat=" + dateFormat +
                ", userId='" + userId + '\'' +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", dateDue=" + dateDue +
                ", dateCreated=" + dateCreated +
                ", completed=" + completed +
                ", firebaseIdUser='" + firebaseIdUser + '\'' +
                ", objectId='" + objectId + '\'' +
                ", priority=" + priority +
                ", hourDuration=" + hourDuration +
                ", minuteDuration=" + minuteDuration +
                ", statusOnLocalStorage='" + statusOnLocalStorage + '\'' +
                '}';
    }
}
