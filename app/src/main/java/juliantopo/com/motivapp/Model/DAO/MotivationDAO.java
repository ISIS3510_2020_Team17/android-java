package juliantopo.com.motivapp.Model.DAO;

import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.mongodb.Block;
import com.mongodb.stitch.android.core.StitchAppClient;
import com.mongodb.stitch.android.core.auth.StitchUser;
import com.mongodb.stitch.android.services.mongodb.remote.RemoteFindIterable;
import com.mongodb.stitch.android.services.mongodb.remote.RemoteMongoClient;
import com.mongodb.stitch.android.services.mongodb.remote.RemoteMongoCollection;
import com.mongodb.stitch.core.services.mongodb.remote.RemoteDeleteResult;
import com.mongodb.stitch.core.services.mongodb.remote.RemoteInsertOneResult;


import org.bson.Document;
import org.bson.types.ObjectId;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import juliantopo.com.motivapp.Model.DTO.MotivationDTO;

public class MotivationDAO implements Serializable {

    // Static atributes
    private static final String LOG = "MotivationDAO";
    private static final String SERVICE_MONGO = "mongodb-atlas";
    private static final String DB_MONGO_NAME = "DBMotivApp";
    private static final String MOTIVATIONS_COLLECTION = "Motivations";

    // Stitch app client
    private static StitchAppClient appClient;
    private static RemoteMongoClient clientMongo;

    // Mongo atlas client
    private static RemoteMongoCollection<Document> collectionMotivations;

    // Current user
    private static StitchUser stitchUser;

    /**
     * Constructor for the motivation DAO
     * @param pAppClient Stitch mongo db client used to connect to the db
     * @param pStitchUser Stitch current user
     */
    public MotivationDAO(StitchAppClient pAppClient, StitchUser pStitchUser){
        this.appClient= pAppClient;
        this.stitchUser = pStitchUser;
        this.clientMongo =  appClient.getServiceClient(RemoteMongoClient.factory, SERVICE_MONGO);
        this.collectionMotivations = clientMongo.getDatabase(DB_MONGO_NAME).getCollection(MOTIVATIONS_COLLECTION);
    }

    /**
     * Posts a new motivation to the collection
     * @param motivationDto The motivation represented in a DTO object
     * @return
     */
    public Task<RemoteInsertOneResult> postMotivation(MotivationDTO motivationDto){
        return collectionMotivations.insertOne(motivationDto.getMongoDbObject()).addOnFailureListener(e -> Log.v(LOG, e.getMessage()));
    }

    /**
     * Gets the motivations from the user with the firebase id given
     * @param idUserFirebase The id from the user
     * @return
     */
    public RemoteFindIterable<Document> getUserMotivations(String idUserFirebase){
        return collectionMotivations.find(new Document("firebase_id_user", idUserFirebase));
    }

    /**
     * Deletes the motivation given the object id of the motivation
     * @param objectId
     * @return
     */
    public Task<RemoteDeleteResult> deleteMotivationById(String objectId){
        return collectionMotivations.deleteOne(new Document("_id", new ObjectId(objectId))).addOnFailureListener(e -> Log.v(LOG, e.getMessage()));
    }

    /**
     * Updates the motivation given its object id and the DTO used to update it
     * @param objectId
     */
    public void updateMotivationById(String objectId, MotivationDTO updatedMotivation){
        collectionMotivations.updateOne(new Document("_id", new ObjectId(objectId)), updatedMotivation.getMongoDbObject());
    }

    /**
     * Returns the motivations from that math the type and user given by parameter
     * @param fireBaseIdUser user to be found
     * @param type type of motivations to be retrieved
     */
    public void getMotivationsByTypeAndUser(String fireBaseIdUser, String type){
        List<MotivationDTO> motivations = new ArrayList<>();
        Block<Document> addBlock = document -> Log.v(LOG, new MotivationDTO(document).toString());
        // Query document
        Document queryDoc = new Document("firebase_id_user", fireBaseIdUser);
        queryDoc.append("type", type);
        collectionMotivations.find(queryDoc).forEach(addBlock);
    }
}
