package juliantopo.com.motivapp.Model.DAO;

import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.mongodb.Block;
import com.mongodb.stitch.android.core.StitchAppClient;
import com.mongodb.stitch.android.core.auth.StitchUser;
import com.mongodb.stitch.android.services.mongodb.remote.RemoteFindIterable;
import com.mongodb.stitch.android.services.mongodb.remote.RemoteMongoClient;
import com.mongodb.stitch.android.services.mongodb.remote.RemoteMongoCollection;
import com.mongodb.stitch.core.services.mongodb.remote.RemoteDeleteResult;
import com.mongodb.stitch.core.services.mongodb.remote.RemoteInsertOneResult;

import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

import juliantopo.com.motivapp.Model.DTO.TaskDTO;

public class TaskDAO {

    // Static atributes
    private static final String LOG = "TaskDAO";
    private static final String SERVICE_MONGO = "mongodb-atlas";
    private static final String DB_MONGO_NAME = "DBMotivApp";
    private static final String TASKS_COLLECTION = "Tasks";

    // Stitch app client
    private static StitchAppClient appClient;
    private static RemoteMongoClient clientMongo;

    // Mongo atlas client
    private static RemoteMongoCollection<Document> collectionTasks;

    // Current user
    private static StitchUser stitchUser;

    /**
     * Constructor for the task DAO
     * @param pAppClient Stitch mongo db client used to connect to the db
     * @param pStitchUser Stitch current user
     */
    public TaskDAO(StitchAppClient pAppClient, StitchUser pStitchUser){
        this.appClient= pAppClient;
        this.stitchUser = pStitchUser;
        this.clientMongo =  appClient.getServiceClient(RemoteMongoClient.factory, SERVICE_MONGO);
        this.collectionTasks = clientMongo.getDatabase(DB_MONGO_NAME).getCollection(TASKS_COLLECTION);
    }

    /**
     * Posts a new task to the collection
     * @param taskDto The task represented in a DTO object
     * @return
     */
    public Task<RemoteInsertOneResult> postTask(TaskDTO taskDto){
        return collectionTasks.insertOne(taskDto.getMongoDbObject()).addOnFailureListener(e -> Log.v(LOG, e.getMessage()));
    }

    /**
     * Gets the tasks from the user with the firebase id given
     * @param idUserFirebase The id from the user
     * @return
     */
    public RemoteFindIterable<Document> getUserTasks(String idUserFirebase){
        return collectionTasks.find(new Document("firebase_id_user", idUserFirebase));
    }

    /**
     * Deletes the tasks given the object id of the task
     * @param objectId
     * @return
     */
    public Task<RemoteDeleteResult> deleteTaskById(String objectId){
        return collectionTasks.deleteOne(new Document("_id", new ObjectId(objectId)));
    }

    /**
     * Updates the task given its object id and the DTO used to update it
     * @param objectId
     */
    public void updateTaskById(String objectId, TaskDTO updatedTask){
        collectionTasks.updateOne(new Document("_id", new ObjectId(objectId)), updatedTask.getMongoDbObject());
    }

    /**
     * Returns the tasks from that math the type and user given by parameter
     * @param fireBaseIdUser user to be found
     * @param type type of motivations to be retrieved
     */
    public void getTasksByTypeAndUser(String fireBaseIdUser, String type){
        List<TaskDTO> tasks = new ArrayList<>();
        Block<Document> addBlock = document -> Log.v(LOG, new TaskDTO(document).toString());
        // Query document
        Document queryDoc = new Document("firebase_id_user", fireBaseIdUser);
        queryDoc.append("type", type);
        collectionTasks.find(queryDoc).forEach(addBlock);
    }
}
