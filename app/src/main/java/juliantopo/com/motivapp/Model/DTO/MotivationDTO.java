package juliantopo.com.motivapp.Model.DTO;

import org.bson.Document;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MotivationDTO {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
    public static final String DELETE = "DELETE";
    public static final String ADD = "ADD_MOTIVATION_";
    // ---------------- Atributes ------------------------------------ //

    // User id
    private String userId;

    // description
    private String description;

    // Type of motivations
    private String type;

    // User ID
    private Date date;

    // Firebase id user
    private String firebaseIdUser;

    // Object id (created by mongo)
    private String objectId;

    // can be null, DELETE or ADD
    private String statusOnLocalStorage;

    // Builder for brand new motivation
    public MotivationDTO(String pUserId, String pType, String pDescripcion, String pFireBaseUser) {
        this.userId = pUserId;
        this.type = pType;
        this.date = new Date();
        this.description = pDescripcion;
        this.firebaseIdUser = pFireBaseUser;
    }

    // Builder for an existing motivation in Mongo db
    public MotivationDTO(Document motivationDocument) {
        this.userId = motivationDocument.getString("user_id");
        this.type = motivationDocument.getString("type");
        this.date = motivationDocument.getDate("date");
        this.description = motivationDocument.getString("description");
        this.firebaseIdUser = motivationDocument.getString("firebase_id_user");
        this.objectId = motivationDocument.getObjectId("_id").toString();
        this.statusOnLocalStorage = "NOTHING";
    }

    /**
     * Constructor for local storage
     */
    public MotivationDTO(String line) {
        String[] attrs = line.split(";");
        this.type = attrs[0];
        this.description = attrs[1];
        //try {
           // this.date = dateFormat.parse(attrs[2]);
       // } catch (ParseException e) {
          //  e.printStackTrace();
      //  }
        this.objectId = attrs[3];
        this.firebaseIdUser = attrs[4];
        this.userId = attrs[5];
        this.statusOnLocalStorage = attrs[6];
    }

    // -------------------------- Setters and getters ---------------------------------------//

    public String getUserId() {
        return userId;
    }

    public MotivationDTO setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getType() {
        return type;
    }

    public MotivationDTO setType(String type) {
        this.type = type;
        return this;
    }

    public Date getDate() {
        return date;
    }

    public MotivationDTO setDate(Date date) {
        this.date = date;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public MotivationDTO setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getFirebaseIdUser() {
        return firebaseIdUser;
    }

    public MotivationDTO setFirebaseIdUser(String firebaseIdUser) {
        this.firebaseIdUser = firebaseIdUser;
        return this;
    }

    public String getObjectId() {
        return objectId;
    }

    public MotivationDTO setObjectId(String objectId) {
        this.objectId = objectId;
        return this;
    }

    public String getStatusOnLocalStorage() {
        return statusOnLocalStorage;
    }

    public void setStatusOnLocalStorage(String statusOnLocalStorage) {
        this.statusOnLocalStorage = statusOnLocalStorage;
    }

    // ---------------------- util ------------------------------------//

    /**
     * Transforms the element to a mongo db document
     *
     * @return Document that represents the DTO
     */
    public Document getMongoDbObject() {
        Document doc = new Document();
        doc.append("user_id", getUserId());
        doc.append("date", getDate());
        doc.append("description", getDescription());
        doc.append("type", getType());
        doc.append("firebase_id_user", getFirebaseIdUser());
        return doc;
    }

    @Override
    public String toString() {
        return "MotivationDTO{" +
                "userId='" + userId + '\'' +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", date=" + date +
                ", firebaseIdUser='" + firebaseIdUser + '\'' +
                ", objectId='" + objectId + '\'' +
                ", statusOnLocalStorage='" + statusOnLocalStorage + '\'' +
                '}';
    }

    /**
     * Returns string to save in local storage file
     *
     * @return string of line to save
     */
    public String toStringLocalStorage() {
        return getType() + ";" + getDescription() + ";" + getDate() + ";" + getObjectId() + ";" + getFirebaseIdUser() + ";" + getUserId() + ";" + this.getStatusOnLocalStorage();
    }
}
