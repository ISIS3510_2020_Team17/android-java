package juliantopo.com.motivapp.Model.DAO;

import android.util.Log;

import com.mongodb.Block;
import com.mongodb.stitch.android.core.StitchAppClient;
import com.mongodb.stitch.android.core.auth.StitchUser;
import com.mongodb.stitch.android.services.mongodb.remote.RemoteFindIterable;
import com.mongodb.stitch.android.services.mongodb.remote.RemoteMongoClient;
import com.mongodb.stitch.android.services.mongodb.remote.RemoteMongoCollection;

import org.bson.Document;
import org.bson.types.ObjectId;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import juliantopo.com.motivapp.Model.DTO.MessageDTO;

public class MessageDAO implements Serializable {

    // Static atributes
    private static final String LOG = "MessageDAO";
    private static final String SERVICE_MONGO = "mongodb-atlas";
    private static final String DB_MONGO_NAME = "DBMotivApp";
    private static final String MESSAGES_COLLECTION = "Messages";

    // Stitch app client
    private static StitchAppClient appClient;
    private static RemoteMongoClient clientMongo;

    // Mongo atlas client
    private static RemoteMongoCollection<Document> collectionMessages;

    // Current user
    private static StitchUser stitchUser;

    /**
     * Constructor for the motivation DAO
     * @param pAppClient Stitch mongo db client used to connect to the db
     * @param pStitchUser Stitch current user
     */
    public MessageDAO(StitchAppClient pAppClient, StitchUser pStitchUser){
        this.appClient= pAppClient;
        this.stitchUser = pStitchUser;
        this.clientMongo =  appClient.getServiceClient(RemoteMongoClient.factory, SERVICE_MONGO);
        this.collectionMessages = clientMongo.getDatabase(DB_MONGO_NAME).getCollection(MESSAGES_COLLECTION);
    }

    /**
     * Posts a new motivation to the collection
     * @param motivationDto The motivation represented in a DTO object
     */
    public void postMessage(MessageDTO motivationDto){
        collectionMessages.insertOne(motivationDto.getMongoDbObject()).addOnFailureListener(e -> Log.v(LOG, e.getMessage()));
    }

    /**
     * Gets the motivations from the user with the firebase id given
     * @param idUserFirebase The id from the user
     * @return
     */
    public RemoteFindIterable<Document> getUserMessages(String idUserFirebase){
        return collectionMessages.find(new Document("firebase_id_user", idUserFirebase));
    }
}
