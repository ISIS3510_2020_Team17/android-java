package juliantopo.com.motivapp.Model.DTO;

import org.bson.Document;

import java.util.Date;

public class MessageDTO {

    // ---------------- Atributes ------------------------------------ //

    // User id
    private String userId;

    // description
    private String message;

    // Type of motivations
    private boolean isUserMessage;

    // User ID
    private Date date;

    // Firebase id user
    private String firebaseIdUser;

    // Object id (created by mongo)
    private String objectId;

    // Builder for brand new motivation
    public MessageDTO(String pUserId, boolean pIsUserMessage, String pMessage, String pFireBaseUser) {
        this.userId = pUserId;
        this.isUserMessage = pIsUserMessage;
        this.date = new Date();
        this.message = pMessage;
        this.firebaseIdUser = pFireBaseUser;
    }

    // Builder for an existing motivation in Mongo db
    public MessageDTO(Document motivationDocument){
        this.userId = motivationDocument.getString("user_id");
        this.isUserMessage = motivationDocument.getBoolean("is_user_message");
        this.date = motivationDocument.getDate("date");
        this.message = motivationDocument.getString("message");
        this.firebaseIdUser = motivationDocument.getString("firebase_id_user");
        this.objectId = motivationDocument.getObjectId("_id").toString();
    }

    public MessageDTO() {}

    // -------------------------- Setters and getters ---------------------------------------//

    public String getUserId() {
        return userId;
    }

    public MessageDTO setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public boolean getIsUserMessage() {
        return isUserMessage;
    }

    public MessageDTO setIsUserMessage(boolean type) {
        this.isUserMessage = type;
        return this;
    }

    public Date getDate() {
        return date;
    }

    public MessageDTO setDate(Date date) {
        this.date = date;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public MessageDTO setMessage(String description) {
        this.message = description;
        return this;
    }

    public String getFirebaseIdUser() {
        return firebaseIdUser;
    }

    public MessageDTO setFirebaseIdUser(String firebaseIdUser) {
        this.firebaseIdUser = firebaseIdUser;
        return this;
    }

    public String getObjectId() {
        return objectId;
    }

    public MessageDTO setObjectId(String objectId) {
        this.objectId = objectId;
        return this;
    }


    // ---------------------- util ------------------------------------//
    /**
     * Transforms the element to a mongo db document
     * @return Document that represents the DTO
     */
    public Document getMongoDbObject(){
        Document doc = new Document();
        doc.append("user_id", getUserId());
        doc.append("date", getDate());
        doc.append("message", getMessage());
        doc.append("is_user_message", getIsUserMessage());
        doc.append("firebase_id_user", getFirebaseIdUser());
        return doc;
    }

    @Override
    public String toString() {
        return "MessageDTO{" +
                "userId='" + userId + '\'' +
                ", message='" + message + '\'' +
                ", isUserMessage=" + isUserMessage +
                ", date=" + date +
                ", firebaseIdUser='" + firebaseIdUser + '\'' +
                ", objectId='" + objectId + '\'' +
                '}';
    }
}
