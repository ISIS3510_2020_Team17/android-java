package juliantopo.com.motivapp.View;


import android.content.Intent;
import android.os.Bundle;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ListView;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import org.bson.Document;


import juliantopo.com.motivapp.Model.DTO.MotivationDTO;
import juliantopo.com.motivapp.R;
import juliantopo.com.motivapp.View.Adapters.CardMotivationArrayAdapter;


public class OverviewMotivations extends Fragment {


    private static final String TAG = "Overview Motivations";
    public CardMotivationArrayAdapter cardArrayAdapter;
    private ActivityMainLogged actividadPadre;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        actividadPadre = (ActivityMainLogged) getActivity();

        cardArrayAdapter = new CardMotivationArrayAdapter((ActivityMainLogged) getActivity(), R.layout.motivation_item, inflater);
        return inflater.inflate(R.layout.motivations_over_view, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        // Set click listener to add motivations btn
        Button btn = getActivity().findViewById(R.id.addMotivationBtn);
        btn.setOnClickListener(view1 -> showDialog());

        // Get list view and initialize the array adapter
        ListView listView = getActivity().findViewById(R.id.card_listViewMotivations);
        listView.addHeaderView(new View(getActivity()));
        listView.addFooterView(new View(getActivity()));

        // Set the listview with the adapter
        listView.setAdapter(cardArrayAdapter);

        // This array will receive the motivations
        actividadPadre.listMotivations = new ArrayMap<>();

        // Getting result from task
        if(actividadPadre.firstTimeLogged) {
            actividadPadre.motivationsTask.addOnSuccessListener(documents -> {

                // Add the motivations to the adapter

                for (Document doc : documents) {
                    MotivationDTO dto = new MotivationDTO(doc);
                    actividadPadre.listMotivations.put(dto.getObjectId(), dto);
                    cardArrayAdapter.add(dto);
                }
                actividadPadre.addMotivationsToLocalStorage(documents);
            });
        }
        else{
            Log.v("TOPOOO", "loaded with storage");
            for (MotivationDTO doc : actividadPadre.getMotivationsLocalStorage()) {
                actividadPadre.listMotivations.put(doc.getObjectId(), doc);
                if(!doc.getStatusOnLocalStorage().equals(MotivationDTO.DELETE)){
                    cardArrayAdapter.add(doc);
                    Log.v("TOPO", doc.getStatusOnLocalStorage());
                }
                else{
                    if(actividadPadre.isConnected()){
                        actividadPadre.initMongoConn().addOnSuccessListener(
                                stitchUser -> {
                                    Log.v("TOPO", "LLEGUEEEEEluilkyuly");
                                    actividadPadre.listMotivations.remove(doc.getObjectId());
                                    actividadPadre.motivationLogic.deleteMotivation(doc.getObjectId());
                                    actividadPadre.updateMotivationsLocalStorage();
                                }
                        );
                    }
                }

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        for (MotivationDTO doc : actividadPadre.getMotivationsLocalStorage()) {
            Log.v("TOPO", doc.toString());
            if(!doc.getStatusOnLocalStorage().equals(MotivationDTO.DELETE)){
                cardArrayAdapter.add(doc);
                if(doc.getStatusOnLocalStorage().equals(MotivationDTO.ADD) && actividadPadre.isConnected()){
                    addMotivWhenConnIsBack(doc);
                }
            }
            else{
                if(actividadPadre.isConnected()){
                    actividadPadre.motivationLogic.deleteMotivation(doc.getObjectId()).addOnSuccessListener(remoteDeleteResult -> {
                        actividadPadre.listMotivations.remove(doc.getObjectId());
                        actividadPadre.updateMotivationsLocalStorage();
                    });
                }
            }
        }

    }

    private void addMotivWhenConnIsBack(MotivationDTO doc){
        actividadPadre.motivationLogic.postMotivation(doc.getType(), doc.getDescription()).addOnSuccessListener(remoteInsertOneResult -> {
            actividadPadre.listMotivations.remove(doc.getObjectId());
            String idInserted = remoteInsertOneResult.getInsertedId().asObjectId().getValue().toString();
            doc.setStatusOnLocalStorage("NOTHING");
            actividadPadre.listMotivations.put(remoteInsertOneResult.getInsertedId().asObjectId().getValue().toString(), doc.setObjectId(idInserted));
            actividadPadre.updateMotivationsLocalStorage();
        });
    }


    public void showDialog() {
        Intent intent = new Intent(getActivity(), Add_motivation_activity.class);
        startActivity(intent);
    }
}
