package juliantopo.com.motivapp.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mongodb.stitch.android.core.Stitch;
import com.mongodb.stitch.android.core.StitchAppClient;
import com.mongodb.stitch.core.services.mongodb.remote.RemoteInsertOneResult;

import org.bson.Document;
import org.imaginativeworld.whynotimagecarousel.CarouselItem;
import org.imaginativeworld.whynotimagecarousel.CarouselOnScrollListener;
import org.imaginativeworld.whynotimagecarousel.CarouselType;
import org.imaginativeworld.whynotimagecarousel.ImageCarousel;
import org.imaginativeworld.whynotimagecarousel.OnItemClickListener;
import org.imaginativeworld.whynotimagecarousel.Utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import juliantopo.com.motivapp.Controller.MotivationLogic;
import juliantopo.com.motivapp.Model.DTO.MotivationDTO;
import juliantopo.com.motivapp.R;

public class Add_motivation_activity extends AppCompatActivity {


    public static final String[] types = new String[]{"Academic", "Health", "Competitive", "Adventure", "Personal", "Media", "Fear based", "Power based"};
    public static final int[] typesImg = new int[]{R.drawable.academical, R.drawable.health, R.drawable.competitive, R.drawable.adventure, R.drawable.personal, R.drawable.media, R.drawable.fear, R.drawable.power};
    public static final int[] typesImgMin = new int[]{R.mipmap.academicmedia_round, R.mipmap.healthmedia_round, R.mipmap.competitivemedia_round, R.mipmap.adventuremedia_round, R.mipmap.personalmedia_round, R.mipmap.mediamotivation_round,
            R.mipmap.fearmedia_round, R.mipmap.powermedia_round};
    public static String[] descriptionCats = new String[types.length];
    public static String[] helpers = new String[types.length];

    private TextView currentCat;
    private TextView currentDescCat;
    private TextInputLayout inputDescriptionLayout;
    private Button buttonAdd;
    private TextInputEditText inputDescription;
    private int currentPosition = 0;
    private MotivationLogic logic;

    public ArrayMap<String, MotivationDTO> listMotivations;

    private ConnectivityManager cm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_motivation_layout);
        setDescriptionCats();
        setHelpers();
        Context context = this;
        cm = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        currentCat = findViewById(R.id.currect_motivation_category);
        currentCat.setText(types[0]);

        currentDescCat = findViewById(R.id.desc_cat_motivation);
        currentDescCat.setText(descriptionCats[0]);

        inputDescriptionLayout = findViewById(R.id.textFieldDescMotiv);
        inputDescriptionLayout.setHelperText(helpers[0]);

        buttonAdd = findViewById(R.id.createMotivBtn);

        inputDescription = findViewById(R.id.input_desc_motivation);

        ImageCarousel carousel = findViewById(R.id.carousel_motivations);

        carousel.setShowTopShadow(false);
        carousel.setTopShadowAlpha(0.6f); // 0 to 1, 1 means 100%
        carousel.setTopShadowHeight(Utils.dpToPx(32, context)); // px value of dp
        carousel.setShowBottomShadow(false);
        carousel.setBottomShadowAlpha(0.6f); // 0 to 1, 1 means 100%
        carousel.setBottomShadowHeight(Utils.dpToPx(64, context)); // px value of dp
        carousel.setShowCaption(true);
        carousel.setCaptionMargin(Utils.dpToPx(16, context)); // px value of dp
        carousel.setCaptionTextSize(Utils.spToPx(16, context)); // px value of sp
        carousel.setIndicatorMargin(Utils.dpToPx(0, context)); // px value of dp
        carousel.setShowNavigationButtons(true);
        carousel.setImageScaleType(ImageView.ScaleType.CENTER);
        carousel.setCarouselBackground(new ColorDrawable(Color.parseColor("#FFFFFF")));
        carousel.setImagePlaceholder(ContextCompat.getDrawable(
                this,
                R.mipmap.taskmedia
        ));
        carousel.setItemLayout(R.layout.custom_fixed_size_item_layout);
        carousel.setImageViewId(R.id.image_view);

        carousel.setPreviousButtonMargin(Utils.dpToPx(8, context)); // px value of dp
        carousel.setNextButtonMargin(Utils.dpToPx(8, context)); // px value of dp
        carousel.setScaleOnScroll(true);
        carousel.setScalingFactor(.15f);
        carousel.setAutoWidthFixing(true);
        carousel.setAutoPlay(false);
        carousel.setOnScrollListener(new CarouselOnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                // ...
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState, int position, @Nullable CarouselItem carouselItem) {
                currentPosition = position;
                currentCat.setText(types[position]);
                currentDescCat.setText(descriptionCats[position]);
                inputDescriptionLayout.setHelperText(helpers[position]);
            }
        });

        //Add images to carousel
        List<CarouselItem> list = new ArrayList<>();

        for (int i = 0; i < types.length; i++) {
            list.add(new CarouselItem(typesImg[i]));
        }
        carousel.addData(list);

        buttonAdd.setOnClickListener(getCreateListener());
        listMotivations = getMotivationsLocalStorage();

    }

    private View.OnClickListener getCreateListener() {
        return view -> {
            String description = this.inputDescription.getText().toString();
            if (description.matches("")) {
                Toast.makeText(this, "Please fill in a description for your motivation", Toast.LENGTH_SHORT).show();
            }
            else{
                // dto
                MotivationDTO dt = new MotivationDTO(FirebaseAuth.getInstance().getUid(), types[currentPosition], description,  FirebaseAuth.getInstance().getUid());


                    //Set status to ADD in local storage
                    dt.setStatusOnLocalStorage(MotivationDTO.ADD);

                    // Create temporal id and set it to the dto
                    String tempId = MotivationDTO.ADD + dt.getDate().getTime();
                    dt.setObjectId(tempId);

                    // Add to local storage
                    listMotivations.put(tempId, dt);
                    addMotivationsToLocalStorage();
                }
                finish();
        };
    }


    // LOCAL STORAGE
    public void addMotivationsToLocalStorage(){
        try {
            FileOutputStream fileout= openFileOutput(ActivityMainLogged.FILE_MOTIVATIONS,Context.MODE_PRIVATE);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
            BufferedWriter b = new BufferedWriter(outputWriter);
            PrintWriter p = new PrintWriter(b, true);
            for(MotivationDTO dto :listMotivations.values()){
                p.println(dto.toStringLocalStorage());
                Log.v("TOPO", "added"+dto.getDescription());
            }

            p.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public ArrayMap<String, MotivationDTO> getMotivationsLocalStorage(){
        ArrayMap motivs = new ArrayMap();
        try {
            FileInputStream fileIn=openFileInput(ActivityMainLogged.FILE_MOTIVATIONS);
            InputStreamReader InputRead= new InputStreamReader(fileIn, StandardCharsets.UTF_8);
            BufferedReader r = new BufferedReader(InputRead);
            r.lines().forEach(line -> motivs.put(line.split(";")[3],new MotivationDTO(line)));
            r.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return motivs;
    }


    public void setDescriptionCats(){
        this.descriptionCats = new String[]{getResources().getString(R.string.academicalDesc),
                getResources().getString(R.string.healthDesc), getResources().getString(R.string.competitiveDesc)
                , getResources().getString(R.string.adventureDesc), getResources().getString(R.string.personalDesc),
                getResources().getString(R.string.mediaDesc), getResources().getString(R.string.fearDesc),
                getResources().getString(R.string.powerDesc)};
    }

    public void setHelpers(){
        this.helpers = new String[]{getResources().getString(R.string.academicalHelper),
                getResources().getString(R.string.healthHelper), getResources().getString(R.string.competitiveHelper)
                , getResources().getString(R.string.adventureHelper), getResources().getString(R.string.personalHelper),
                getResources().getString(R.string.mediaHelper), getResources().getString(R.string.fearHelper),
                getResources().getString(R.string.powerHelper)};
    }
}