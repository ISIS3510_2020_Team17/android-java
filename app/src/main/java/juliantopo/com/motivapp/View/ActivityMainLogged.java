package juliantopo.com.motivapp.View;

;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.util.ArrayMap;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mongodb.stitch.android.core.Stitch;
import com.mongodb.stitch.android.core.StitchAppClient;
import com.mongodb.stitch.android.core.auth.StitchUser;
import com.mongodb.stitch.core.auth.providers.anonymous.AnonymousCredential;

import org.bson.Document;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import juliantopo.com.motivapp.Controller.MotivationLogic;
import juliantopo.com.motivapp.Controller.TaskLogic;
import juliantopo.com.motivapp.Model.DTO.MotivationDTO;
import juliantopo.com.motivapp.Model.DTO.TaskDTO;
import juliantopo.com.motivapp.R;
import juliantopo.com.motivapp.Controller.ShakeDetectorLogic;
import juliantopo.com.motivapp.View.Adapters.CardTaskArrayAdapter;


public class ActivityMainLogged extends AppCompatActivity {

    // Local storage file where motivations are placed
    public final static String FILE_MOTIVATIONS = "motivations.txt";

    // Local storage file where tasks are placed
    public final static String FILE_TASKS = "tasks.txt";

    // Key for the shared preference value that represents whether if it is the first time the user logs to the app or not
    public final static String SHARED_PREF_FIRST_TIME = "first_time";

    public FirebaseUser currentUser;

    // The following are used for the shake detection
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetectorLogic mShakeDetector;

    // Motivations logic
    public MotivationLogic motivationLogic;
    public Task<ArrayList<Document>> motivationsTask;

    // Tasks logic
    public TaskLogic taskLogic;
    public Task<ArrayList<Document>> tasksTask;
    public CardTaskArrayAdapter cardArrayAdapter;

    public boolean firstTime = true;

    // Boolean that reprents whether it is the first time the user logged to the device
    public boolean firstTimeLogged;

    // Shared preferences
    public SharedPreferences sharedPrefs = null;

    // List of motivations
    public ArrayMap<String, MotivationDTO> listMotivations;

    // List of tasks
    public ArrayMap<String, TaskDTO> listTasks;

    // Connectivity eventuality manager
    private ConnectivityManager cm;

    // Mongo stitch app client
    public StitchAppClient stitchAppClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_logged);

        cardArrayAdapter = new CardTaskArrayAdapter(this, R.layout.task_item, this.getLayoutInflater());

        // Init connectivity manager
        cm = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        // Configure and create botton nav
        configureBottonNav();

        // detect if first time getting here
        isFirstTimeLogged();

        // ShakeDetector initialization
        initSensor();

        // Welcome alert
        showWelcomeAlert();

        //Get current user
        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        //Mongo
        initMongoConn().addOnSuccessListener(
                stitchUser -> {
                    // Motivation logic
                    motivationLogic = new MotivationLogic(currentUser.getUid(), stitchAppClient, stitchUser);
                    motivationsTask = motivationLogic.getMotivationsFromUser(new ArrayList<>());

                    // Task logic
                    taskLogic = new TaskLogic(currentUser.getUid(), stitchAppClient, stitchUser);
                    tasksTask = taskLogic.getTasksFromUser(new ArrayList<>());

                    if(firstTimeLogged){
                        updateUIWithTasks(tasksTask);
                    }

                    firstTime = false;
                }
        );
    }

    /**
     * Init the conn with mongo db
     * @return task of connection with stitch user
     */
    public Task<StitchUser> initMongoConn(){
        String resMongo = getApplicationContext().getResources().getString(R.string.MongoTopo);
        if (!Stitch.hasAppClient(resMongo)){
            Stitch.initializeDefaultAppClient(resMongo);
        }
        stitchAppClient = Stitch.getDefaultAppClient();

        return stitchAppClient.getAuth().loginWithCredential(new AnonymousCredential());
    }



    private void updateUIWithTasks(Task<ArrayList<Document>> pTasksTask)
    {
        ListView listView = findViewById(R.id.card_listViewTasks);
        listView.addHeaderView(new View(this));
        listView.addFooterView(new View(this));

        // Set the listview with the adapter
        listView.setAdapter(cardArrayAdapter);

        // This array will receive the motivations
        ArrayList<Document> docs;
        listTasks = new ArrayMap<>();
        // Getting result from task
        pTasksTask.addOnSuccessListener(documents -> {
            // Add the motivations to the adapter
            for (Document doc : documents) {
                TaskDTO dto = new TaskDTO(doc);
                cardArrayAdapter.add(dto);
                listTasks.put(dto.getObjectId(), dto);
            }
            addTasksToLocalStorage(documents);
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        // Set shared preference of first log in to false
        if (firstTimeLogged) {
            sharedPrefs.edit().putBoolean(SHARED_PREF_FIRST_TIME, false).apply();
        }

        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
    }

    // --------------------------------- CONNECTIVITY UTIL METHODS ---------------------------------

    /**
     * Returns whether the device is connected or not
     * @return true if connected
     */
    public boolean isConnected(){
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    // --------------------------------- LOCAL STORAGE UTIL METHODS ---------------------------------

    /**
     * Adds the list of motivations to the local storage file
     * @param motivations list of motivations
     */
    public void addMotivationsToLocalStorage(ArrayList<Document> motivations){
        try {
            FileOutputStream fileout= openFileOutput(FILE_MOTIVATIONS,Context.MODE_PRIVATE);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
            BufferedWriter b = new BufferedWriter(outputWriter);
            PrintWriter p = new PrintWriter(b, true);
            for (Document motivation : motivations) {
                p.println(new MotivationDTO(motivation).toStringLocalStorage());
            }
            p.close();
        } catch (Exception ignored) {
            sharedPrefs.edit().putBoolean(SHARED_PREF_FIRST_TIME, true).apply();
        }
    }

    /**
     * Updates the files with the current list of motivations
     */
    public void updateMotivationsLocalStorage(){
        try {
            FileOutputStream fileout= openFileOutput(FILE_MOTIVATIONS,Context.MODE_PRIVATE);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
            BufferedWriter b = new BufferedWriter(outputWriter);
            PrintWriter p = new PrintWriter(b, true);
            for (MotivationDTO motivation : listMotivations.values()) {
                p.println(motivation.toStringLocalStorage());
            }
            p.close();
        } catch (Exception ignored) {
            sharedPrefs.edit().putBoolean(SHARED_PREF_FIRST_TIME, true).apply();
        }
    }

    /**
     * Obtains the motivations stored in the file
     * @return list of motivations
     */
    public List<MotivationDTO> getMotivationsLocalStorage(){
        List<MotivationDTO> motivs = new ArrayList<>();
        try {
            FileInputStream fileIn=openFileInput(FILE_MOTIVATIONS);
            InputStreamReader InputRead= new InputStreamReader(fileIn, StandardCharsets.UTF_8);
            BufferedReader r = new BufferedReader(InputRead);
            r.lines().forEach(line -> motivs.add(new MotivationDTO(line)));
            r.close();
        } catch (Exception e) {
            sharedPrefs.edit().putBoolean(SHARED_PREF_FIRST_TIME, true).apply();
            e.printStackTrace();
        }
        return motivs;
    }

    /**
     * Adds the list of tasks to the local storage file
     * @param tasks list of tasks
     */
    public void addTasksToLocalStorage(ArrayList<Document> tasks){
        try {
            FileOutputStream fileout= openFileOutput(FILE_TASKS,Context.MODE_PRIVATE);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
            BufferedWriter b = new BufferedWriter(outputWriter);
            PrintWriter p = new PrintWriter(b, true);
            for (Document task : tasks) {
                p.println(new TaskDTO(task).toStringLocalStorage());
            }
            p.close();
        } catch (Exception ignored) {
            sharedPrefs.edit().putBoolean(SHARED_PREF_FIRST_TIME, true).apply();
        }
    }

    /**
     * Updates the files with the current list of tasks
     */
    public void updateTasksLocalStorage(){
        try {
            FileOutputStream fileout= openFileOutput(FILE_TASKS,Context.MODE_PRIVATE);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
            BufferedWriter b = new BufferedWriter(outputWriter);
            PrintWriter p = new PrintWriter(b, true);
            for (TaskDTO task : listTasks.values()) {
                p.println(task.toStringLocalStorage());
            }
            p.close();
        } catch (Exception ignored) {
            sharedPrefs.edit().putBoolean(SHARED_PREF_FIRST_TIME, true).apply();
        }
    }

    /**
     * Obtains the tasks stored in the file
     * @return list of tasks
     */
    public List<TaskDTO> getTasksLocalStorage(){
        List<TaskDTO> tasks = new ArrayList<>();
        try {
            FileInputStream fileIn=openFileInput(FILE_TASKS);
            InputStreamReader InputRead= new InputStreamReader(fileIn, StandardCharsets.UTF_8);
            BufferedReader r = new BufferedReader(InputRead);
            r.lines().forEach(line -> tasks.add(new TaskDTO(line)));
            r.close();
        } catch (Exception e) {
            sharedPrefs.edit().putBoolean(SHARED_PREF_FIRST_TIME, true).apply();
            e.printStackTrace();
        }
        return tasks;
    }


    ////////////////////

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void updateTaskGetMotivations(){
        if(motivationLogic != null){
            motivationsTask = motivationLogic.getMotivationsFromUser(new ArrayList<>());
        }
    }

    public void updateTaskGetTasks(){
        if(taskLogic != null){
            tasksTask = taskLogic.getTasksFromUser(new ArrayList<>());
        }
    }

    @Override
    public void onPause() {
        // Add the following line to unregister the Sensor Manager onPause
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }

    /**
     * Opens the chat bot using an intent
     */
    public void openChatbot() {
        Intent intent = new Intent(this, ChatBot.class);
        startActivity(intent);
    }

    /**
     * Handles shake event
     */
    private void handleShakeEvent() {
        openChatbot();
    }

    private void initSensor() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetectorLogic();
        mShakeDetector.setOnShakeListener(count -> handleShakeEvent());
    }

    private void configureBottonNav() {
        BottomNavigationView navView = findViewById(R.id.nav_view);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_motivations, R.id.navigation_calendar, R.id.navigation_profs)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
    }

    private void showWelcomeAlert() {
        if(firstTimeLogged) {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle(getApplicationContext().getResources().getString(R.string.app_name));
            alertDialog.setMessage(getApplicationContext().getResources().getString(R.string.weKnow));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    (dialog, which) -> dialog.dismiss());
            alertDialog.show();
        }
    }

    private void isFirstTimeLogged() {

        sharedPrefs = getPreferences(Context.MODE_PRIVATE);
        firstTimeLogged = sharedPrefs.getBoolean(SHARED_PREF_FIRST_TIME, true);
        Log.v("FIRST", String.valueOf(firstTimeLogged));
    }
}