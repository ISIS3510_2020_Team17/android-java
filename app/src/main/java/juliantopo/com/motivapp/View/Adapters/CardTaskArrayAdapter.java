package juliantopo.com.motivapp.View.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.List;


import juliantopo.com.motivapp.Controller.TaskLogic;
import juliantopo.com.motivapp.Model.DTO.MotivationDTO;
import juliantopo.com.motivapp.Model.DTO.TaskDTO;
import juliantopo.com.motivapp.R;
import juliantopo.com.motivapp.View.ActivityMainLogged;
import juliantopo.com.motivapp.View.Add_motivation_activity;
import juliantopo.com.motivapp.View.Add_task_activity;

public class CardTaskArrayAdapter extends ArrayAdapter<TaskDTO> {

    private static final String TAG = "CardTaskArrayAdapter";
    private List<TaskDTO> cardList = new ArrayList<>();
    private LayoutInflater inflater;
    private ActivityMainLogged activity;
    
    static class CardViewHolder {
        TextView line1;
        TextView line2;
        Button editBtn;
        Button deleteBtn;
        ImageView imgView;

    }

    public CardTaskArrayAdapter(ActivityMainLogged pActivity, int textViewResourceId, LayoutInflater pInflater) {
        super(pActivity.getApplicationContext(), textViewResourceId);
        this.inflater = pInflater;
    }
    

    @Override
    public void add(TaskDTO object) {
        if(!contains(object.getObjectId())){
            cardList.add(object);
            super.add(object);
        }
    }

    private boolean contains(String objectId){
        for (int i = 0; i < cardList.size(); i++) {
            if(cardList.get(i).getObjectId().equals(objectId)){
                return true;
            }
        }
        return  false;
    }

    public void remove(TaskDTO object, int position) {
        super.remove(object);
        cardList.remove(position);
    }

    @Override
    public int getCount() {
        return this.cardList.size();
    }

    @Override
    public TaskDTO getItem(int index) {
        return this.cardList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CardTaskArrayAdapter.CardViewHolder viewHolder;
        if (row == null) {
            row = inflater.inflate(R.layout.task_item, parent, false);
            viewHolder = new CardTaskArrayAdapter.CardViewHolder();
            viewHolder.line1 = (TextView) row.findViewById(R.id.typeTaskItem);
            viewHolder.line2 = (TextView) row.findViewById(R.id.descriptionTaskItem);
            viewHolder.editBtn = (Button) row.findViewById(R.id.editTaskBtn);
            viewHolder.deleteBtn = (Button) row.findViewById(R.id.deleteTaskBtn);
            row.setTag(viewHolder);

            viewHolder.imgView = row.findViewById(R.id.typeTaskImage);
            row.setTag(viewHolder);
            MaterialCardView cardMat = (MaterialCardView) row;

            cardMat.setOnLongClickListener(getOnLongClickListener(cardMat));

        } else {
            viewHolder = (CardTaskArrayAdapter.CardViewHolder) row.getTag();
        }
        TaskDTO card = getItem(position);
        viewHolder.line1.setText(card.getType());
        viewHolder.line2.setText(card.getDescription());

        viewHolder.imgView.setImageResource(Add_task_activity.typesImgMin[findResource(card.getType())]);


        addListenerOnRemoveButton(viewHolder.deleteBtn, card, position);

        return row;
    }

    private int findResource(String type) {
        String[] arr = Add_task_activity.types;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i].equals(type)){
                return i;
            }
        }
        return 0;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    private View.OnLongClickListener getOnLongClickListener(MaterialCardView cardView) {
        return view -> {
            cardView.findViewById(R.id.optionsTask).setVisibility(cardView.isChecked() ? View.INVISIBLE : View.VISIBLE);
            cardView.setChecked(!cardView.isChecked());
            return true;
        };
    }

    private void addListenerOnRemoveButton(Button editBtn, TaskDTO task, int position) {
        editBtn.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(inflater.getContext());
            builder.setMessage(inflater.getContext().getResources().getString(R.string.confirmationDeleteTask))
                    .setCancelable(false)
                    .setPositiveButton("Remove", (dialog, id) -> {
                        activity.updateTaskGetTasks();

                        // Removes from adapter
                        remove(task, position);

                        // If the device has internet, then the task is removed from the adapter, from the database and from the local storage file
                        if(activity.isConnected()){
                            // Removes from logic
                            if(activity.taskLogic!= null){
                                activity.taskLogic.deleteTask(task.getObjectId());
                            }

                            // Removes from local storage
                            this.activity.listTasks.remove(task.getObjectId());
                        }
                        // If device is not connected to the internet, then the task is removed from the adapted. In local storage it is given an attribute signaling
                        // that this task should be removed from the database as soon as the internet comes back and from local storage.
                        else{
                            // If the task has not been added to the db (it was added offline and device is still offline) then just it just removes the task from local storage
                            if(task.getObjectId().equals(TaskDTO.ADD)){
                                this.activity.listTasks.remove(task.getObjectId());
                            }
                            // Adds to task that signals it must be deleted from db when internet comes back
                            else{
                                this.activity.listTasks.get(task.getObjectId()).setStatusOnLocalStorage(TaskDTO.DELETE);
                            }
                        }
                        this.activity.updateTasksLocalStorage();
                    })

                    .setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.cancel());
            AlertDialog alert = builder.create();
            alert.setTitle("Delete task");

            // change color of the button
            alert.setOnShowListener(arg0 -> alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(inflater.getContext().getColor(R.color.colorDanger)));
            alert.show();
        });
    }

}
