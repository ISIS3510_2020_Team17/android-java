package juliantopo.com.motivapp.View;

import android.app.AppComponentFactory;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import juliantopo.com.motivapp.R;

public class email_password_signin_activity extends AppCompatActivity {

    EditText emailID, password ;
    Button btnSignUp;
    TextView tvSignIn;
    FirebaseAuth mFirebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.email_password_signin_activity);

        mFirebaseAuth = FirebaseAuth.getInstance();
        emailID = findViewById(R.id.editTextTextEmailAddress);
        password = findViewById(R.id.editTextTextPassword);
        tvSignIn = findViewById(R.id.textView6);
        btnSignUp = findViewById(R.id.button);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailID.getText().toString();
                String pwd = password.getText().toString();
                if (email.isEmpty()) {
                    emailID.setError("Please enter a valid email");
                    emailID.requestFocus();
                }
                else if (pwd.isEmpty()) {
                    password.setError("Please enter a valid password");
                    password.requestFocus();
                }
                else if (email.isEmpty() && pwd.isEmpty())
                {
                    Toast.makeText(email_password_signin_activity.this,"Fields are empty!",Toast.LENGTH_SHORT).show();

                }
                else if (!(email.isEmpty() && pwd.isEmpty()))
                {
                    mFirebaseAuth.createUserWithEmailAndPassword(email,pwd).addOnCompleteListener(email_password_signin_activity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (! task.isSuccessful())
                            {
                                Toast.makeText(email_password_signin_activity.this,"SignUp unsuccesful. Please try again.", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                startActivity(new Intent(email_password_signin_activity.this, ActivityMainLogged.class));
                            }
                        }
                    });
                }
                else
                {
                   Toast.makeText(email_password_signin_activity.this,"Error occurred!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(email_password_signin_activity.this, LogInActivity.class);
                startActivity(i);
            }
        });
    }
}