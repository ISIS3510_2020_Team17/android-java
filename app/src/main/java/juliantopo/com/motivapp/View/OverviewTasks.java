package juliantopo.com.motivapp.View;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;

import org.bson.Document;

import java.util.ArrayList;
import java.util.Date;

import juliantopo.com.motivapp.Controller.DonutLogic;
import juliantopo.com.motivapp.Model.DTO.MotivationDTO;
import juliantopo.com.motivapp.Model.DTO.TaskDTO;
import juliantopo.com.motivapp.R;
import juliantopo.com.motivapp.View.Adapters.CardTaskArrayAdapter;


public class OverviewTasks extends Fragment {

    /**
     * Handles current user
     */
    public FirebaseUser currentUser;

    /**
     * Handles the donut
     */
    public DonutLogic doughnut;

    private static final String TAG = "Overview Tasks";

    private ActivityMainLogged actividadPadre;
    private CardTaskArrayAdapter cardArrayAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.tasks_over_view, container, false);
        cardArrayAdapter = new CardTaskArrayAdapter((ActivityMainLogged) getActivity(), R.layout.task_item, inflater);
        //Get current user
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        return root;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.actividadPadre = (ActivityMainLogged) getActivity();
        // update tasks in ui
        //updateTasksUI();
        if(!actividadPadre.firstTime){
            updateUIWithTasks(actividadPadre.tasksTask);
        }
        // Set click listener to add motivations btn
        Button btn = getActivity().findViewById(R.id.addTaskBtn);
        btn.setOnClickListener(view1 -> showDialog());

        ArrayMap<String, TaskDTO> d = new ArrayMap<>();
        d.put("1",new TaskDTO("123","Health","Una descripción",1,"kaak",new Date(), 3, 2, 15));
        d.put("2",new TaskDTO("123","Health","Una descripción",0,"kaak",new Date(), 3, 2, 15));
        updateUI((int) getPercentageTasksDone(d), (int) getTimeMissing(d));
    }

    private void updateUIWithTasks(Task<ArrayList<Document>> pTasksTask)
    {
        ListView listView = actividadPadre.findViewById(R.id.card_listViewTasks);
        listView.addHeaderView(new View(actividadPadre));
        listView.addFooterView(new View(actividadPadre));

        // Set the listview with the adapter
        listView.setAdapter(cardArrayAdapter);

        // This array will receive the motivations
        actividadPadre.listTasks = new ArrayMap<>();

        // Getting result from task
        if(actividadPadre.firstTimeLogged) {
            pTasksTask.addOnSuccessListener(documents -> {

                // Add the motivations to the adapter
                for (Document doc : documents) {
                    TaskDTO dto = new TaskDTO(doc);
                    actividadPadre.listTasks.put(dto.getObjectId(), dto);
                    cardArrayAdapter.add(dto);
                }
                actividadPadre.addTasksToLocalStorage(documents);
            });
        }
        else{
            Log.v(TAG, "loaded with storage");
            for (TaskDTO doc : actividadPadre.getTasksLocalStorage()) {
                actividadPadre.listTasks.put(doc.getObjectId(), doc);
                if(!doc.getStatusOnLocalStorage().equals(TaskDTO.DELETE)){
                    cardArrayAdapter.add(doc);
                    Log.v(TAG, doc.getStatusOnLocalStorage());
                }
                else{
                    if(actividadPadre.isConnected()){
                        actividadPadre.initMongoConn().addOnSuccessListener(
                                stitchUser -> {
                                    Log.v("TOPO", "LLEGUEEEEEluilkyuly");
                                    actividadPadre.listTasks.remove(doc.getObjectId());
                                    actividadPadre.taskLogic.deleteTask(doc.getObjectId());
                                    actividadPadre.updateTasksLocalStorage();
                                }
                        );
                    }
                }

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!actividadPadre.firstTime){
            for (TaskDTO doc : actividadPadre.getTasksLocalStorage()) {
                Log.v(TAG, doc.toString());
                if(!doc.getStatusOnLocalStorage().equals(TaskDTO.DELETE)){
                    cardArrayAdapter.add(doc);
                    if(doc.getStatusOnLocalStorage().equals(MotivationDTO.ADD) && actividadPadre.isConnected()){
                        addTaskWhenConnIsBack(doc);
                    }
                }
                else{
                    if(actividadPadre.isConnected()){
                        actividadPadre.taskLogic.deleteTask(doc.getObjectId()).addOnSuccessListener(remoteDeleteResult -> {
                            actividadPadre.listMotivations.remove(doc.getObjectId());
                            actividadPadre.updateMotivationsLocalStorage();
                        });
                    }
                }
            }
        }

    }


    private void addTaskWhenConnIsBack(TaskDTO doc){
        actividadPadre.taskLogic.postTask(doc.getType(), doc.getDescription(), doc.getDateDue(), doc.getPriority(), doc.getHourDuration(), doc.getMinuteDuration()).addOnSuccessListener(remoteInsertOneResult -> {
            actividadPadre.listTasks.remove(doc.getObjectId());
            String idInserted = remoteInsertOneResult.getInsertedId().asObjectId().getValue().toString();
            doc.setStatusOnLocalStorage("NOTHING");
            actividadPadre.listTasks.put(remoteInsertOneResult.getInsertedId().asObjectId().getValue().toString(), doc.setObjectId(idInserted));
            actividadPadre.updateTasksLocalStorage();
        });
    }


    private void updateUI(int percentage, int hours)
    {
        // Create donut
        createDoughnut(percentage);

        // Make UI changes given the user
        if (currentUser != null) {
            for (UserInfo profile : currentUser.getProviderData()) {
                // Id of the provider (ex: google.com)
                String providerId = profile.getProviderId();

                // UID specific to the provider
                String uid = profile.getUid();

                // Name, email address, and profile photo Url
                String name = profile.getDisplayName();
                String email = profile.getEmail();
                Uri photoUrl = profile.getPhotoUrl();

                // Replace USER with user's name in welcome text
                TextView welcomeUser = (TextView) getActivity().findViewById(R.id.welcome_user);
                String nameText;
                if(name != null){
                    nameText = name;
                }
                else if(email != null){
                    nameText = email.split("@")[0];
                }
                else{
                    nameText = "";
                }
                welcomeUser.setText(welcomeUser.getText().toString().replace("USER", nameText));

                // Replace the percentage in the text view
                TextView percentageTv = (TextView) getActivity().findViewById(R.id.percentageTasks);
                percentageTv.setText(percentage + "%");

                TextView timeTasks = (TextView) getActivity().findViewById(R.id.textview_second4);
                timeTasks.setText(timeTasks.getText().toString().replace("3h",hours+"h"));
            }
        }
    }


    /**
     * Inits the donut
     */
    private void createDoughnut(int percentageTasksCompleted){
        doughnut = getActivity().findViewById(R.id.doughnut);
        doughnut.animateSetPercent(percentageTasksCompleted);
    }

    /**
     * Returns the percentage of daily tasks done
     * @return
     * @param dtos
     */
    private double getPercentageTasksDone(ArrayMap<String, TaskDTO> dtos){
        double completedTasks = 0;
        double percentageOfTasksDone;
        for (TaskDTO dto: dtos.values())
        {
            if(dto.getCompleted() == 1){
                completedTasks ++ ;
            }
        }
        percentageOfTasksDone = completedTasks/dtos.size()*100.0;
        return percentageOfTasksDone;
    }

    /**
     * Returns the percentage of daily tasks done
     * @return
     * @param dtos
     */
    private double getTimeMissing(ArrayMap<String, TaskDTO> dtos){
        double timeMissingMinutes = 0.0;
        for (TaskDTO dto: dtos.values())
        {
            if(dto.getCompleted() == 0){
                timeMissingMinutes += dto.getHourDuration()*60.0 + dto.getMinuteDuration();
            }
        }
        double timeMissingHours = (timeMissingMinutes/60.0)+1.0;
        return timeMissingHours;
    }


    /**
     * Show the add task dialog in fullscreen
     */
    public void showDialog() {
        Intent intent = new Intent(getActivity(), Add_task_activity.class);
        startActivity(intent);
    }
}
