package juliantopo.com.motivapp.View.Adapters;


import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.ImageView;
import android.widget.TextView;



import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.List;

import juliantopo.com.motivapp.Controller.MotivationLogic;
import juliantopo.com.motivapp.Model.DTO.MotivationDTO;
import juliantopo.com.motivapp.R;
import juliantopo.com.motivapp.View.ActivityMainLogged;
import juliantopo.com.motivapp.View.Add_motivation_activity;

public class CardMotivationArrayAdapter extends ArrayAdapter<MotivationDTO> {
    private static final String TAG = "CardArrayAdapter";
    private List<MotivationDTO> cardList = new ArrayList<>();
    private LayoutInflater inflater;
    private ActivityMainLogged activity;


    static class CardViewHolder {
        TextView line1;
        TextView line2;
        Button editBtn;
        Button deleteBtn;

        //
        ImageView imgView;
    }

    public CardMotivationArrayAdapter(ActivityMainLogged pActivity, int textViewResourceId, LayoutInflater pInflater) {
        super(pActivity.getApplicationContext(), textViewResourceId);
        this.activity = pActivity;
        this.inflater = pInflater;
    }


    @Override
    public void add(MotivationDTO object) {
        if(!contains(object.getObjectId())){
            cardList.add(object);
            super.add(object);
        }
    }

    private boolean contains(String objectId){
        for (int i = 0; i < cardList.size(); i++) {
            if(cardList.get(i).getObjectId().equals(objectId)){
                return true;
            }
        }
        return  false;
    }

    public void remove(MotivationDTO object, int position) {
        super.remove(object);
        cardList.remove(position);
    }

    @Override
    public int getCount() {
        return this.cardList.size();
    }

    @Override
    public MotivationDTO getItem(int index) {
        return this.cardList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CardViewHolder viewHolder;
        if (row == null) {
            row = inflater.inflate(R.layout.motivation_item, parent, false);
            viewHolder = new CardViewHolder();
            viewHolder.line1 = row.findViewById(R.id.typeMotivationItem);
            viewHolder.line2 = row.findViewById(R.id.descriptionMotivationItem);
            viewHolder.editBtn = row.findViewById(R.id.editMotivationBtn);
            viewHolder.deleteBtn = row.findViewById(R.id.deleteMotivationBtn);

            //
            viewHolder.imgView = row.findViewById(R.id.typeMotivationImage);
            row.setTag(viewHolder);
            MaterialCardView cardMat = (MaterialCardView) row;

            cardMat.setOnLongClickListener(getOnLongClickListener(cardMat));

        } else {
            viewHolder = (CardViewHolder) row.getTag();
        }
        MotivationDTO card = getItem(position);
        viewHolder.line1.setText(card.getType());
        viewHolder.line2.setText(card.getDescription());
        //
        viewHolder.imgView.setImageResource(Add_motivation_activity.typesImgMin[findResource(card.getType())]);
        addListenerOnRemoveButton(viewHolder.deleteBtn, card, position);
        return row;
    }

    //
    private int findResource(String type) {
        String[] arr = Add_motivation_activity.types;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i].equals(type)){
                return i;
            }
        }
        return 0;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    private View.OnLongClickListener getOnLongClickListener(MaterialCardView cardView) {
        return view -> {
            cardView.findViewById(R.id.optionsMotivation).setVisibility(cardView.isChecked() ? View.INVISIBLE : View.VISIBLE);
            cardView.setChecked(!cardView.isChecked());
            return true;
        };
    }

    private void addListenerOnRemoveButton(Button editBtn, MotivationDTO motiv, int position) {
        editBtn.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(inflater.getContext());
            builder.setMessage(inflater.getContext().getResources().getString(R.string.confirmationDeleteMotivation))
                    .setCancelable(false)
                    .setPositiveButton("Remove", (dialog, id) -> {
                        activity.updateTaskGetMotivations();

                        // Removes from adapter
                        remove(motiv, position);

                        // If the device has internet, then the motivation is removed from the adapter, from the database and from the local storage file
                        if(activity.isConnected()){
                            // Removes from logic
                            if(activity.motivationLogic!= null){
                                activity.motivationLogic.deleteMotivation(motiv.getObjectId());
                            }

                            // Removes from local storage
                            this.activity.listMotivations.remove(motiv.getObjectId());
                        }
                        // If device is not connected to the internet, then the motivation is removed from the adapted. In local storage it is given an attribute signaling
                        // that this motivation should be removed from the database as soon as the internet comes back and from local storage.
                        else{
                            // If the motivation has not been added to the db (it was added offline and device is still offline) then just it just removes the motivation from local storage
                            if(motiv.getObjectId().equals(MotivationDTO.ADD)){
                                this.activity.listMotivations.remove(motiv.getObjectId());
                            }
                            // Adds to motivation that signals it must be deleted from db when internet comes back
                            else{
                                this.activity.listMotivations.get(motiv.getObjectId()).setStatusOnLocalStorage(MotivationDTO.DELETE);
                            }
                        }
                        this.activity.updateMotivationsLocalStorage();
                    })

                    .setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.cancel());
            AlertDialog alert = builder.create();
            alert.setTitle("Delete motivation");

            // change color of the button
            alert.setOnShowListener(arg0 -> alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(inflater.getContext().getColor(R.color.colorDanger)));
            alert.show();
        });
    }
}