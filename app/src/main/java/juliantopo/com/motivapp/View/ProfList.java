package juliantopo.com.motivapp.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import juliantopo.com.motivapp.R;

public class ProfList extends Fragment {
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_prof, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button button = getActivity().findViewById(R.id.go_chat_bot);
        button.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), ChatBot.class);
            startActivity(intent);
        });
    }
}