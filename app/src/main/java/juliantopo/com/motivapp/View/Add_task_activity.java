package juliantopo.com.motivapp.View;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.ArrayMap;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.mongodb.stitch.android.core.Stitch;
import com.mongodb.stitch.android.core.StitchAppClient;

import org.imaginativeworld.whynotimagecarousel.CarouselItem;
import org.imaginativeworld.whynotimagecarousel.CarouselOnScrollListener;
import org.imaginativeworld.whynotimagecarousel.ImageCarousel;
import org.imaginativeworld.whynotimagecarousel.Utils;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import juliantopo.com.motivapp.Controller.TaskLogic;
import juliantopo.com.motivapp.Model.DTO.MotivationDTO;
import juliantopo.com.motivapp.Model.DTO.TaskDTO;
import juliantopo.com.motivapp.R;

public class Add_task_activity extends AppCompatActivity {


    public static final String[] types = new String[]{"Academic", "Health", "Competitive", "Adventure", "Personal", "Media", "Fear based", "Power based"};
    public static final int[] typesImg = new int[]{R.drawable.academical, R.drawable.health, R.drawable.competitive, R.drawable.adventure, R.drawable.personal, R.drawable.media, R.drawable.fear, R.drawable.power};
    public static final int[] typesImgMin = new int[]{R.mipmap.academicmedia_round, R.mipmap.healthmedia_round, R.mipmap.competitivemedia_round, R.mipmap.adventuremedia_round, R.mipmap.personalmedia_round, R.mipmap.mediamotivation_round,
            R.mipmap.fearmedia_round, R.mipmap.powermedia_round};
    public static String[] descriptionCats = new String[types.length];
    public static String[] helpers = new String[types.length];

    private TextView currentCat;
    private TextView currentDescCat;
    private TextInputLayout inputDescriptionLayout;
    private TextInputEditText inputPriority;
    private TextInputEditText inputDate;
    private TextInputLayout layoutDate;
    private TextInputLayout layoutPriority;
    private TimePicker pickerTime;
    private Button buttonAdd;
    private TextInputEditText inputDescription;
    private int currentPosition = 0;
    private TaskLogic logic;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");

    public ArrayMap<String, TaskDTO> listTasks;

    private ConnectivityManager cm;


    MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
    MaterialDatePicker<Long> picker = builder.build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_task_layout);
        setDescriptionCats();
        setHelpers();
        Context context = this;

        cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        currentCat = findViewById(R.id.currect_task_category);
        currentCat.setText(types[0]);

        currentDescCat = findViewById(R.id.desc_cat_task);
        currentDescCat.setText(descriptionCats[0]);

        inputDescriptionLayout = findViewById(R.id.textFieldDescTask);
        inputDescriptionLayout.setHelperText(helpers[0]);

        buttonAdd = findViewById(R.id.createTaskBtn);

        inputDate = findViewById(R.id.input_date_task);
        layoutDate = findViewById(R.id.dateFieldTask);

        inputPriority = findViewById(R.id.priority_task);
        layoutPriority = findViewById(R.id.priorityFieldTask);

        pickerTime = findViewById(R.id.timePicker1);
        pickerTime.setIs24HourView(true);

        ImageButton btnDate = findViewById(R.id.pickDate);
        btnDate.setOnClickListener(getDialog());

        inputDescription = findViewById(R.id.input_desc_task);

        ImageCarousel carousel = findViewById(R.id.carousel_tasks);

        carousel.setShowTopShadow(false);
        carousel.setTopShadowAlpha(0.6f); // 0 to 1, 1 means 100%
        carousel.setTopShadowHeight(Utils.dpToPx(32, context)); // px value of dp
        carousel.setShowBottomShadow(false);
        carousel.setBottomShadowAlpha(0.6f); // 0 to 1, 1 means 100%
        carousel.setBottomShadowHeight(Utils.dpToPx(64, context)); // px value of dp
        carousel.setShowCaption(true);
        carousel.setCaptionMargin(Utils.dpToPx(16, context)); // px value of dp
        carousel.setCaptionTextSize(Utils.spToPx(16, context)); // px value of sp
        carousel.setIndicatorMargin(Utils.dpToPx(0, context)); // px value of dp
        carousel.setShowNavigationButtons(true);
        carousel.setImageScaleType(ImageView.ScaleType.CENTER);
        carousel.setCarouselBackground(new ColorDrawable(Color.parseColor("#FFFFFF")));
        carousel.setImagePlaceholder(ContextCompat.getDrawable(
                this,
                R.mipmap.taskmedia
        ));
        carousel.setItemLayout(R.layout.custom_fixed_size_item_layout);
        carousel.setImageViewId(R.id.image_view);

        carousel.setPreviousButtonMargin(Utils.dpToPx(8, context)); // px value of dp
        carousel.setNextButtonMargin(Utils.dpToPx(8, context)); // px value of dp
        carousel.setScaleOnScroll(true);
        carousel.setScalingFactor(.15f);
        carousel.setAutoWidthFixing(true);
        carousel.setAutoPlay(false);
        carousel.setOnScrollListener(new CarouselOnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                // ...
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState, int position, @Nullable CarouselItem carouselItem) {
                currentPosition = position;
                currentCat.setText(types[position]);
                currentDescCat.setText(descriptionCats[position]);
                inputDescriptionLayout.setHelperText(helpers[position]);
            }
        });

        //Add images to carousel
        List<CarouselItem> list = new ArrayList<>();

        for (int i = 0; i < types.length; i++) {
            list.add(new CarouselItem(typesImg[i]));
        }
        carousel.addData(list);

        buttonAdd.setOnClickListener(getCreateListener());
        listTasks = getTasksLocalStorage();
    }

    /**
     * Dialog for date picker
     *
     * @return
     */
    private View.OnClickListener getDialog() {
        return view -> {
            picker.show(getSupportFragmentManager(), picker.toString());
            picker.addOnPositiveButtonClickListener(selection -> {
                Date date = new Date(selection);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                inputDate.setText(dateFormat.format(date));
            });
        };
    }

    /**
     * Listener for the add button
     *
     * @return
     */
    private View.OnClickListener getCreateListener() {
        return view -> {
            layoutDate.setError(null);
            inputDescriptionLayout.setError(null);
            layoutPriority.setError(null);
            String priority = inputPriority.getText().toString();
            Date date = null;
            try {
                date = dateFormat.parse(inputDate.getText().toString());
            } catch (ParseException e) {
                layoutDate.setError("Invalid format\nUse: MMM dd, yyyy\ne.g. Nov 21, 2021");
            }
            if (!checkInputs(priority, date)) {
                return;
            } else {
                // dto
                TaskDTO dt = new TaskDTO(FirebaseAuth.getInstance().getUid(), types[currentPosition],
                        this.inputDescription.getText().toString(), 0, FirebaseAuth.getInstance().getUid(),
                        date, Integer.parseInt(inputPriority.getText().toString()), pickerTime.getHour(), pickerTime.getMinute());


                //Set status to ADD in local storage
                dt.setStatusOnLocalStorage(TaskDTO.ADD);

                // Create temporal id and set it to the dto
                String tempId = TaskDTO.ADD + dt.getDateCreated().getTime();
                dt.setObjectId(tempId);

                // Add to local storage
                listTasks.put(tempId, dt);
                addTasksToLocalStorage();
            }
            finish();
        };
    }

    /**
     * Checks if all inputs are valid. If they are the method returns true
     *
     * @return true or false whether inputs are correct
     */
    private boolean checkInputs(String priority, Date date) {
        boolean correct = true;
        if (inputDate.getText().toString().trim().isEmpty()) {
            correct = false;
            layoutDate.setError("Date is empty");
        } else if (date != null && new Date().after(date)) {
            correct = false;
            layoutDate.setError("Date has already passed");
        }
        if (inputDescription.getText().toString().trim().isEmpty()) {
            correct = false;
            inputDescriptionLayout.setError("Description is empty");
        }


        if (priority.equals("")) {
            correct = false;
            layoutPriority.setError("Priority is empty");
        } else if (Integer.parseInt(priority) > 5 || Integer.parseInt(priority) < 1) {
            correct = false;
            layoutPriority.setError("Priority must be between 1 and 5");
        }
        return correct;
    }

    // LOCAL STORAGE
    public void addTasksToLocalStorage() {
        try {
            FileOutputStream fileout = openFileOutput(ActivityMainLogged.FILE_TASKS, Context.MODE_PRIVATE);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
            BufferedWriter b = new BufferedWriter(outputWriter);
            PrintWriter p = new PrintWriter(b, true);
            for (TaskDTO dto : listTasks.values()) {
                p.println(dto.toStringLocalStorage());
                Log.v("TOPO", "added" + dto.getDescription());
            }

            p.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public ArrayMap<String, TaskDTO> getTasksLocalStorage() {
        ArrayMap tasks = new ArrayMap();
        try {
            FileInputStream fileIn = openFileInput(ActivityMainLogged.FILE_TASKS);
            InputStreamReader InputRead = new InputStreamReader(fileIn, StandardCharsets.UTF_8);
            BufferedReader r = new BufferedReader(InputRead);
            r.lines().forEach(line -> tasks.put(line.split(";")[8], new TaskDTO(line)));
            r.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tasks;
    }


    public void setDescriptionCats() {

        this.descriptionCats = new String[]{getResources().getString(R.string.academicalDescTask),
                getResources().getString(R.string.healthDescTask), getResources().getString(R.string.competitiveDescTask)
                , getResources().getString(R.string.adventureDescTask), getResources().getString(R.string.personalDescTask),
                getResources().getString(R.string.mediaDescTask), getResources().getString(R.string.fearDescTask),
                getResources().getString(R.string.powerDescTask)};
    }

    public void setHelpers() {
        this.helpers = new String[]{getResources().getString(R.string.academicalHelperTask),
                getResources().getString(R.string.healthHelperTask), getResources().getString(R.string.competitiveHelperTask)
                , getResources().getString(R.string.adventureHelperTask), getResources().getString(R.string.personalHelperTask),
                getResources().getString(R.string.mediaHelperTask), getResources().getString(R.string.fearHelperTask),
                getResources().getString(R.string.powerHelperTask)};

    }
}
