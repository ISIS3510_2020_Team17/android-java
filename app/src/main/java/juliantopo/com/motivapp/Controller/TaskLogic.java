package juliantopo.com.motivapp.Controller;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.mongodb.stitch.android.core.StitchAppClient;
import com.mongodb.stitch.android.core.auth.StitchUser;
import com.mongodb.stitch.core.services.mongodb.remote.RemoteDeleteResult;
import com.mongodb.stitch.core.services.mongodb.remote.RemoteInsertOneResult;

import org.bson.Document;

import java.util.ArrayList;
import java.util.Date;

import juliantopo.com.motivapp.Model.DAO.TaskDAO;
import juliantopo.com.motivapp.Model.DTO.TaskDTO;

public class TaskLogic {

    private ArrayList<TaskDTO> tasks = new ArrayList<>();

    private String currentUser;
    private StitchUser stitchUser;
    private StitchAppClient stitchAppClient;
    private TaskDAO taskDAO;

    /**
     * Creates the logic for the motivation resource
     *
     * @param currentUser     from firebase
     * @param stitchAppClient mongo stich client
     * @param stitchUser      mongo anonymous user
     */
    public TaskLogic(String currentUser, StitchAppClient stitchAppClient, StitchUser stitchUser) {
        this.currentUser = currentUser;
        this.stitchAppClient = stitchAppClient;
        this.stitchUser = stitchUser;
        taskDAO = new TaskDAO(stitchAppClient, stitchUser);
    }

    /**
     * Posts a task
     *
     * @param pType
     * @param pDescripcion
     * @return true if posted, false if not posted
     */
    public Task<RemoteInsertOneResult> postTask(String pType, String pDescripcion, Date date, int priority, int hours, int minutes) {
        if (pType != null && pDescripcion != null && pDescripcion.length() > 0) {
            return this.taskDAO.postTask(new TaskDTO(this.stitchUser.getId(), pType, pDescripcion, 0, currentUser, date, priority, hours, minutes));
        }
        return null;
    }

    /**
     * Deletes a motivation
     *
     * @param objectId
     * @return true if deleted, false if not deleted
     */
    public Task<RemoteDeleteResult> deleteTask(String objectId) {

        return this.taskDAO.deleteTaskById(objectId);


    }

    /**
     * Obtains motivations from current user and stores them in a list
     *
     * @return
     */
    public Task<ArrayList<Document>> getTasksFromUser(ArrayList<Document> res) {
        return this.taskDAO.getUserTasks(this.currentUser).into(res);
    }

    public ArrayList<TaskDTO> getMotivations() {
        return tasks;
    }

    public void setMotivations(ArrayList<TaskDTO> motivations) {
        this.tasks = motivations;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public StitchUser getStitchUser() {
        return stitchUser;
    }

    public void setStitchUser(StitchUser stitchUser) {
        this.stitchUser = stitchUser;
    }

    public StitchAppClient getStitchAppClient() {
        return stitchAppClient;
    }

    public void setStitchAppClient(StitchAppClient stitchAppClient) {
        this.stitchAppClient = stitchAppClient;
    }

    public TaskDAO getTaskDAO() {
        return taskDAO;
    }

    public void setTaskDAO(TaskDAO taskDAO) {
        this.taskDAO = taskDAO;
    }
}
