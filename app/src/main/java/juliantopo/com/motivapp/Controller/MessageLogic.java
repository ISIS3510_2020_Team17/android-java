package juliantopo.com.motivapp.Controller;

import com.google.android.gms.tasks.Task;
import com.mongodb.stitch.android.core.StitchAppClient;
import com.mongodb.stitch.android.core.auth.StitchUser;

import org.bson.Document;

import java.util.ArrayList;

import juliantopo.com.motivapp.Model.DAO.MessageDAO;
import juliantopo.com.motivapp.Model.DTO.MessageDTO;

public class MessageLogic {

    private String currentUser;
    private StitchUser stitchUser;
    private StitchAppClient stitchAppClient;
    private MessageDAO messageDAO;

    /**
     * Creates the logic for the motivation resource
     * @param currentUser from firebase
     * @param stitchAppClient mongo stich client
     * @param stitchUser mongo anonymous user
     */
    public MessageLogic(String currentUser, StitchAppClient stitchAppClient, StitchUser stitchUser) {
        this.currentUser = currentUser;
        this.stitchAppClient = stitchAppClient;
        this.stitchUser = stitchUser;
        messageDAO = new MessageDAO(stitchAppClient, stitchUser);
    }

    /**
     * Posts a motivation
     * @param isFromUser
     * @param pMessage
     * @return true if posted, false if not posted
     */
    public boolean postMessage(boolean isFromUser, String pMessage){
        if(pMessage != null && pMessage.length() > 0){
            this.messageDAO.postMessage(new MessageDTO(this.stitchUser.getId(), isFromUser, pMessage, currentUser));
            return true;
        }
       return false;
    }

    /**
     * Obtains motivations from current user and stores them in a list
     * @return
     */
    public Task<ArrayList<Document>> getMessagesFromUser(ArrayList<Document> res){
        return this.messageDAO.getUserMessages(this.currentUser).into(res);
    }


    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public StitchUser getStitchUser() {
        return stitchUser;
    }

    public void setStitchUser(StitchUser stitchUser) {
        this.stitchUser = stitchUser;
    }

    public StitchAppClient getStitchAppClient() {
        return stitchAppClient;
    }

    public void setStitchAppClient(StitchAppClient stitchAppClient) {
        this.stitchAppClient = stitchAppClient;
    }

    public MessageDAO getMessageDAO() {
        return messageDAO;
    }

    public void setMessageDAO(MessageDAO motivationDAO) {
        this.messageDAO = motivationDAO;
    }
}
