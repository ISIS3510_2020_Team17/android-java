package juliantopo.com.motivapp.Controller;

import com.google.android.gms.tasks.Task;
import com.mongodb.stitch.android.core.StitchAppClient;
import com.mongodb.stitch.android.core.auth.StitchUser;
import com.mongodb.stitch.core.services.mongodb.remote.RemoteDeleteResult;
import com.mongodb.stitch.core.services.mongodb.remote.RemoteInsertOneResult;

import org.bson.Document;

import java.util.ArrayList;

import juliantopo.com.motivapp.Model.DAO.MotivationDAO;
import juliantopo.com.motivapp.Model.DTO.MotivationDTO;

public class MotivationLogic{
    private ArrayList<MotivationDTO> motivations = new ArrayList<>();

    private String currentUser;
    private StitchUser stitchUser;
    private StitchAppClient stitchAppClient;
    private MotivationDAO motivationDAO;

    /**
     * Creates the logic for the motivation resource
     * @param currentUser from firebase
     * @param stitchAppClient mongo stich client
     * @param stitchUser mongo anonymous user
     */
    public MotivationLogic(String currentUser, StitchAppClient stitchAppClient, StitchUser stitchUser) {
        this.currentUser = currentUser;
        this.stitchAppClient = stitchAppClient;
        this.stitchUser = stitchUser;
        motivationDAO = new MotivationDAO(stitchAppClient, stitchUser);
    }

    /**
     * Posts a motivation
     * @param pType
     * @param pDescripcion
     * @return true if posted, false if not posted
     */
    public Task<RemoteInsertOneResult> postMotivation(String pType, String pDescripcion){
        if(pType != null && pDescripcion != null && pDescripcion.length() > 0){
            return this.motivationDAO.postMotivation(new MotivationDTO(this.stitchUser.getId(), pType, pDescripcion, currentUser));
        }
        return null;
    }

    /**
     * Deletes a motivation
     * @param objectId
     * @return true if deleted, false if not deleted
     */
    public Task<RemoteDeleteResult> deleteMotivation(String objectId){
            return this.motivationDAO.deleteMotivationById(objectId);
    }

    /**
     * Obtains motivations from current user and stores them in a list
     * @return
     */
    public Task<ArrayList<Document>> getMotivationsFromUser(ArrayList<Document> res){
        return this.motivationDAO.getUserMotivations(this.currentUser).into(res);
    }

    public ArrayList<MotivationDTO> getMotivations() {
        return motivations;
    }

    public void setMotivations(ArrayList<MotivationDTO> motivations) {
        this.motivations = motivations;
    }






    //// Setters and getters

}
